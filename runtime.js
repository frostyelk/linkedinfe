/**
 Copyright 2015 Frosty Elk AB
 */
/* jshint -W069, -W097, -W117 */
/* global IN*/
// ECMAScript 5 strict mode
"use strict";

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

/////////////////////////////////////
// Plugin class
cr.plugins_.LinkedInFE = function (runtime) {
    this.runtime = runtime;
};


//noinspection UnterminatedStatementJS
(function () {

    var pluginProto = cr.plugins_.LinkedInFE.prototype;

    /////////////////////////////////////
    // Object type class
    pluginProto.Type = function (plugin) {
        this.plugin = plugin;
        this.runtime = this.plugin.runtime;
    };

    var typeProto = pluginProto.Type.prototype;

    // Plugin common vars
    var linkedInReady = false;
    //var conditionTag = {};
    var linkedInInst = {};
    var linkedInRuntime = {};
    var lastError = "";

    // LinkedIn JS API
    var linkedInSDKURL = "http://platform.linkedin.com/in.js";
    var linkedInApiKey = "75h6pg629yke32";
    var linkedInAuthorize = true;


    // called on startup for each object type
    typeProto.onCreate = function () {

    };

    /////////////////////////////////////
    // Instance class
    pluginProto.Instance = function (type) {
        this.type = type;
        this.runtime = this.type.runtime;
    };

    var instanceProto = pluginProto.Instance.prototype;

    // called whenever an instance is created
    instanceProto.onCreate = function () {


        linkedInInst = this;
        linkedInRuntime = this.runtime;

        // Setup the LinkedIn JS API
        // https://developer.linkedin.com/docs/js-sdk
        (function (d) {
            var js = d.createElement('script');
            js.src = linkedInSDKURL;
            var options = "\n" + "api_key: " + linkedInApiKey + "\n";
            options += "authorize: " + linkedInAuthorize + "\n";
            js.text = options;
            d.getElementsByTagName('head')[0].appendChild(js);
        }(document));

        document.addEventListener('resume', function () {
            console.log("LinkedIn FE resume: ");
        });


        linkedInRuntime.tickMe(linkedInInst);
    };


    instanceProto.tick = function () {
        // Wait for the LinkedIn SDK to load
        if (linkedInRuntime.running_layout) {
            if (typeof window["IN"] === 'undefined') {
                console.warn("LinkedIn FE not created, the LinkedIn SDK is missing");
                linkedInRuntime.untickMe(linkedInInst);
                linkedInReady = false;
            } else {
                //console.log("LinkedIn FE running with LinkedIn SDK");
                if (typeof IN["User"] === 'undefined') {
                    //console.log("No LinkedIn User Object, waiting...");
                } else {
                    console.log("LinkedIn User Object available");
                    linkedInRuntime.untickMe(linkedInInst);
                    linkedInReady = true;
                    linkedInRuntime.trigger(cr.plugins_.LinkedInFE.prototype.cnds.onReady, linkedInInst);
                }
            }
        }
    };


    // The comments around these functions ensure they are removed when exporting, since the
    // debugger code is no longer relevant after publishing.
    /**BEGIN-PREVIEWONLY**/
    instanceProto.getDebuggerValues = function (propsections) {
        // Append to propsections any debugger sections you want to appear.
        // Each section is an object with two members: "title" and "properties".
        // "properties" is an array of individual debugger properties to display
        // with their name and value, and some other optional settings.
        propsections.push({
            "title": "My debugger section",
            "properties": [
                // Each property entry can use the following values:
                // "name" (required): name of the property (must be unique within this section)
                // "value" (required): a boolean, number or string for the value
                // "html" (optional, default false): set to true to interpret the name and value
                //									 as HTML strings rather than simple plain text
                // "readonly" (optional, default false): set to true to disable editing the property

                // Example:
                // {"name": "My property", "value": this.myValue}
            ]
        });
    };

    instanceProto.onDebugValueEdited = function (header, name, value) {
        // Called when a non-readonly property has been edited in the debugger. Usually you only
        // will need 'name' (the property name) and 'value', but you can also use 'header' (the
        // header title for the section) to distinguish properties with the same name.
        /*
         if (name === "My property") {
         this.myProperty = value;
         }
         */

    };
    /**END-PREVIEWONLY**/

    //////////////////////////////////////
    // Conditions
    function Cnds() {
    }

    Cnds.prototype.isReady = function () {
        return linkedInReady;
    };

    Cnds.prototype.onReady = function () {
        return true;
    };

    Cnds.prototype.isUserLoggedIn = function () {
        return IN["User"]["isAuthorized"]();
    };

    pluginProto.cnds = new Cnds();


    //////////////////////////////////////
    // Actions
    function Acts() {
    }

    Acts.prototype.logIn = function () {
        if (!linkedInReady) {
            return;
        }

        IN["User"]["authorize"](function(result){
            console.log("IN['User']['authorize'] result: " + result);
            console.log("Is User Authorized?: " + IN["User"]["isAuthorized"]());

        }, linkedInInst);

    };

    Acts.prototype.logOut = function () {
        if (!linkedInReady) {
            return;
        }

        IN["User"]["logout"](function(result){
            console.log("IN['User']['logout'] result: " + result);
            console.log("Is User Authorized?: " + IN["User"]["isAuthorized"]());

        }, linkedInInst);


    };

    // Acts here

    pluginProto.acts = new Acts();

    //////////////////////////////////////
    // Expressions
    function Exps() {
    }

    Exps.prototype.LastError = function (ret) {
        ret.set_string(lastError);
    };


    pluginProto.exps = new Exps();

}
());